# OpenML dataset: ulaanbaatar-weather-2015-2020

https://www.openml.org/d/43487

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Context
You can find a detailed weather data (2015-2020) of Ulaanbaatar, capital city of Mongolia.
Content
Data is including the timestamps (UTC) and timely basis data of weather related features, such as temperature, wind etc
Inspiration
Since there are a lot of need to use weather data but not enough free materials, I am sharing this for you guys!
Data Description
You can find it easily on https://www.wunderground.com/
Enjoy and Upvote 3

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43487) of an [OpenML dataset](https://www.openml.org/d/43487). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43487/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43487/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43487/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

